from flask import Flask, request, send_file, json
from werkzeug.contrib.fixers import ProxyFix
import hashlib
import os

app = Flask(__name__)
static = os.path.abspath('store')


def save_file(file):
    file_name = hashlib.sha256(file).hexdigest()
    folder = os.path.join(static, file_name[:2])
    if not os.path.exists(folder):
        os.mkdir(folder)

    with open(os.path.join(folder, file_name), 'wb') as f:
        f.write(file)
    return file_name


def file_src(file_name):
    file_path = os.path.join(static, file_name[:2], file_name)
    if os.path.exists(file_path):
        return file_path
    return False


@app.route('/api/upload', methods=['POST'])
def upload():
    if 'data' in request.files and request.files['data'] != []:
        file = request.files['data'].read()
        file_name = save_file(file)
        response = app.response_class(
            response=json.dumps({
                'success': 1,
                'response': {
                    'file_name': file_name
                }
            }),
            status=200,
            mimetype='application/json'
        )
    else:
        response = app.response_class(
            response=json.dumps({
                'success': 0,
                'response': "'data' parameter is empty"
            }),
            status=400,
            mimetype='application/json'
        )
    return response


@app.route('/api/download/<file_name>', methods=['GET'])
def download(file_name=None):
    src = file_src(file_name)
    if src:
        response = send_file(src)
    else:
        response = app.response_class(
            response=json.dumps({
                'success': 0,
                'response': 'file does not exist'
            }),
            status=404,
            mimetype='application/json'
        )
    return response


@app.route('/api/delete/<file_name>', methods=['GET'])
def delete(file_name=None):
    src = file_src(file_name)
    if src:
        os.remove(src)
        response = app.response_class(
            response=json.dumps({
                'success': 1,
                'response': 'removed'
            }),
            status=200,
            mimetype='application/json'
        )
    else:
        response = app.response_class(
            response=json.dumps({
                'success': 0,
                'response': 'file does not exist'
            }),
            status=404,
            mimetype='application/json'
        )
    return response


app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == '__main__':
    app.run()

***Run as daemon:***

```gunicorn http_daemon:app --daemon ```

## API
**Upload:**

`curl -F 'data=@<FILE_NAME>' http://127.0.0.1:8000/api/upload`

return output in JSON format:

``
{
'success': 1,
'response': {
	'file_name': <HASH>
}}
``

**Download:**

`wget http://127.0.0.1:8000/api/download/<HASH>`

**Delete:**

` curl -i http://127.0.0.1:8000/api/delete/<HASH>`

return output in JSON format:

``
{
'success': 1,
'response': 'removed'
}
``